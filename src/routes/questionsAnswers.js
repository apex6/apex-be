import scraping from '../utils/scraping.js'


const opts = {
    schema: {
        response: {
            200: {
                type: 'object',
                properties: {
                    qa: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                question: {type: 'string'},
                                answer: {type: 'string'}
                            }
                        }
                    },
                    updatedTime: {
                        type: 'string'
                    }
                }
            },
            500: {
                description: 'Generic server error',
                type: 'object',
                properties: {
                  error: { type: 'string' },
                  statusMessage: { type: 'string' },
                  statusCode: { type: 'number' },
                  externalError: { type: 'string' },
                  externalCode: { type: 'string' },
                  message: { type: 'string' },
                  body: { type: 'string' },
                  xTrackId: { type: 'string' },
                }
              },
              504: {
                description: 'Gateway Timeout Error',
                type: 'object',
                properties: {
                  error: { type: 'string' },
                  statusMessage: { type: 'string' },
                  statusCode: { type: 'number' },
                  externalError: { type: 'string' },
                  externalCode: { type: 'string' },
                  message: { type: 'string' },
                  body: { type: 'string' },
                  xTrackId: { type: 'string' },
                }
              }
        }
    }
}

export default async function(app) {
    app.get('/questionsAnswers', opts, async function(req, reply) {
        try {
            const qa = await scraping()
            return reply.send({qa, updatedTime: Date.now()})
        } catch (error) {
            console.log(error)
        }
    })
}