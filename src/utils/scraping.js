'use strict'

import got from 'got'
import $ from 'cheerio' 

const url = 'https://www.naranja.com/comercios-amigos'

export default async function () {
    const html = await got(url)
    const questions = questionsScraping(html)
    const answers = answersScraping(html)
    
    return questions.map((question, i) =>
        ({
            question: question,
            answer: answers[i]
        })
    )
}

const questionsScraping = html => {
    let questions = []
    $('.faq-title_question', html.body).map((i, e) => { 
        questions.push($(e).text())
    })
    return questions
}

const answersScraping = html => {
    let answers = []
    $('.faq-text', html.body).map((i, e) => { 
        answers.push($(e).find('p').text())
    })
    return answers
}
